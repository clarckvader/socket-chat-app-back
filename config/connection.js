const mysql = require('mysql'),
      data = require('./dataConnection.json'),
      objectConecction= {
            host     : data.mysql.host,
            user     : data.mysql.user,
            password : data.mysql.password,
            database : data.mysql.database

        };

const con = mysql.createConnection(objectConecction);

con.connect((err) => {
    if(err){
        console.log(`Ha ocurrido un error: ${err}`)
    }else{
        console.log(`Conexion exitosa`)
    }
     
});

export default con;

