const express = require('express');
const moment = require('moment');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server, {cors: {
  origin:"http://127.0.0.1:8000"}});


var sockets = {};

const mysql = require('mysql'),
      data = require('./config/dataConnection.json'),
      objectConecction= {
            host     : data.mysql.host,
            user     : data.mysql.user,
            password : data.mysql.password,
            database : data.mysql.database

        };

const con = mysql.createConnection(objectConecction);

con.connect((err) => {
    if(err){
        console.log(`Ha ocurrido un error: ${err}`)
    }else{
        console.log(`Conexion exitosa`)
    }
     
});



io.on('connection', (socket) => {
    if(!sockets[socket.handshake.query.user_id]){
        socket[socket.handshake.query.user_id] = [];
    }
    socket[socket.handshake.query.user_id].push(socket);
    socket.broadcast.emit('user_connected', socket.handshake.query.user_id);

    con.query(`Update users Set is_online=1 where id=${socket.handshake.query.user_id}`, (err) =>{
      if(err)
        throw err;
      console.log("user Connected", socket.handshake.query.user_id )
    });

    

    socket.on('disconnect', (err) => {
      socket.broadcast.emit('user_disconnected',socket.handshake.query.user_id )
      for(var index in sockets[socket.handshake.query.user_id]){
        if(socket.id == socket[socket.handshake.query.user_id][index].id){
          sockets[socket.handshake.query.user_id].splice(index,1)
        }
      }
      con.query(`Update users Set is_online=0 where id=${socket.handshake.query.user_id}`, (err, res) =>{
        if(err)
          throw err;
        console.log("user disconnected", socket.handshake.query.user_id )
      });
    });
});

const port = 3000;

server.listen(port, () => {
  console.log(`Server running on port${port}`)
});